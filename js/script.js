let n = +prompt('Enter a number:')

while (n === 0){
  n = +prompt(`Error! ${n} cannot be used. Enter another number:`)
}

function getFibonacci(f0, f1, n){
  if (n < -1)
    return getFibonacci(f1 - f0, f0, n + 1)

  if (n === -1)
    return f1 - f0
    
  if (n < 3)
    return getFibonacci.arguments[n-1]

  if (n === 3)
    return f0 + f1 

  if (n > 3)
    return getFibonacci(f1, f0 + f1, n-1)
}

console.log(getFibonacci(34, 55, n));



// ----- без отрицательных чисел и проверок ввода getFibonacci в одну строку ----- //

getFibonacci = (f0, f1, n) => n == 3 ? f0 + f1 : getFibonacci(f1, f0 + f1, n-1)

console.log(getFibonacci(3, 5, 5));
